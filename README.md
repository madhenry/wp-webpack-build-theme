# Webpack Wordpress theme starter

* Make sure you have Node.js and NPM / Yarn installed
* Run `npm i` to install npm packages
* Make any necessary changes to the config at scripts/webpack.config.js and package.json scripts when using browser-sync with a proxy
* `npm start` to develop & `npm run build` when building for production


> Hope it's useful and submit any pull requests if you make any improvements!
> 
> ***Henry***
